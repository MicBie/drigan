Quick start
===========

Installation
------------

This document describes the process of setting up the environment. At the end 
of it there is an attachement with a quick list of commands that you need to 
run on some systems to get started even faster.

Pre-requirements
^^^^^^^^^^^^^^^^

Altough most requirements will be installed for you automatically using 
`PIP <https://github.com/pypa/pip>`_, there are some pre-requirements:

 * `Python <https://www.python.org/>`_ >= 3.4
 * `PostgreSQL <http://www.postgresql.org/>`_ >= 9.3.4 with `HStore
   <http://www.postgresql.org/docs/9.0/static/hstore.html>`_ enabled
 * `git <http://git-scm.com/>`_
 * `Mercurial <http://mercurial.selenic.com/>`_

Enabling HStore
"""""""""""""""

To enable HStore in Postgresql type:

.. code-block:: bash

    $ psql -d template1 -c 'create extension hstore;'

From now on all created databases would have HStore installed. You can also
run this command only for one database, after creating it.

Setting up virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is recommended to use virtual environments to decouple Python packages. For
example, using `venv <https://docs.python.org/3/library/venv.html>`_ (included
in Python >= 3.3):

.. code-block:: bash

    $ pyvenv /path/to/environment        #  create virtual environment
    $ cd /path/to/environment            #  cd to its directory
    $ source bin/activate                #  activate virtual environment

From now you can use only packages installed in this virtual environment.
additionally, copies of `python` and `pip` binaries were created.

Take a look at `virtualenvwrapper <http://virtualenvwrapper.readthedocs.org/en/latest/>`_
to make those commands even simpler.

Downloading project
^^^^^^^^^^^^^^^^^^^

Clone it using git:

.. code-block:: bash

    $ git clone https://bitbucket.org/zeroos/drigan.git

Installing requirements
^^^^^^^^^^^^^^^^^^^^^^^

All requirements are in ``requirements.txt``. You can install it with (don't
forget about activating virtual environment!):

.. code-block:: bash

    $ pip install -r requirements.txt

Hooray! Everything is installed, time to configure it and run.

Setup
-----

Like in every Django application, you have to provide a ``settings.py`` file.
There is a template for it in ``drigan/settings_example.py``, just copy it and
edit with your favourite editor:

.. code-block:: bash

    $ cp drigan/settings_example.py drigan/settings.py
    $ vim drigan/settings.py

Creating database
^^^^^^^^^^^^^^^^^


Now it's time to create a database. If you are doing it on your own just for
development purposes you can for example use the following command. You have to
issue it as a user with permissions to create PostgreSQL databases on your
system (usually ``postgres``).

.. code-block:: bash

    postgres $ createdb drigan     # or other database name

If you ever need to reset your database to initial state you can ofcourse use
Django management command (``reset``) or just recreate the database (probably more
reliable):

.. code-block:: bash

    postgres $ dropdb drigan     # drop the database
    postgres $ createdb drigan     # and create it again

Before first stable version is released we are not going to use migrations, so
you will have to reset the database after each model change.


Postgresql priveleges configuration
-----------------------------------

Since postgresql privelege configuration can be akward here is a quide.

Postgresql security config is in ``pg_hba.conf`` file, and by default
(on ubuntu and debian) local socket connections use ``peer`` authentication
so current sysytem user is named ``foo`` when connecting postgres will
automatically authenticate ``foo`` database user. For this to work you'll need:

In ``settings.py`` set:

* ``HOST`` to empty string
* ``DATABASE`` to drigan
* ``USERNAME`` to ``foo`` (your system username)
* ``PASSWORD`` to empty string


* Add postgresql user that has the same username as you. In example we assume
  that your system username is foo.

  .. code-block:: bash

        sudo createuser foo

  If you want to give your user superuser rights in postgres:

  .. code-block:: bash

        sudo createuser -s foo

* Create database:

  .. code-block:: bash

        sudo createdb -O foo drigan

settings.py
^^^^^^^^^^^

Every setting in the copied ``settings_example.py`` file is documented, so you can
just go through them and adjust them.

If you are just trying to run it in developing mode, you don't have to
change much -- just adjust your database credentials if needed and everything
should work.

However, if you'd like to set up a production environment, you should look over
each setting. And don't forget to set ``DEBUG = False``!

Database
^^^^^^^^

.. code-block:: bash

    $ python ./manage.py syncdb

Collecting static files
^^^^^^^^^^^^^^^^^^^^^^^

.. note::

    You don't have to do it when ``DEBUG = False``, i.e. in a development
    environmennt. In this case static files are served automatically by Django.

Before doing it make sure ``STATIC_ROOT`` is set correctly in ``settings.py``.

.. code-block:: bash

    $ python manage.py collectstatic

That's it!
----------

And that's everything. If you're just running development instance you can run
the server with

.. code-block:: bash

    $ python manage.py runserver

and start coding!

If you are setting up a production environment you can use any technique that's
used to `deploy Django <https://docs.djangoproject.com/en/dev/howto/deployment/>`_.

Testing
-------

Django is recreating test database prior to each test run. This has unfortunate
side-effect that ``hstore`` extension is missing. Until someone fixes this
error you'll need to create ``hstore`` extension in ``template1`` database.
If you do this **all future databases created in this system will contain
this extension.**

.. code-block:: bash

    psql template1 -c 'create extension hstore;'


List of commands needed to configure environment on some systems
================================================================

Debian/Ubuntu
-------------

1. sudo apt-get install python3.4-dev
2. sudo apt-get install python-pip
3. sudo pip install -U pip
4. sudo apt-get install postgresql-9.3
5. sudo apt-get install postgresql-contrib-9.3
6. sudo apt-get install postgresql-server-dev-9.3
7. sudo su - postgres
8. psql -d template1 -c 'create extension hstore;'
<ctrl+D> to logout from postgres
9. sudo apt-get install mercurial
10. sudo apt-get install git
11. git config --global user.name "FIRST_NAME LAST_NAME"
12. git config --global user.email "MY_NAME@example.com"
13. sudo apt-get install python-virtualenv
14. mkdir ~/Drigan
15. cd ~/Drigan
16. virtualenv --python=/usr/bin/python3.4 environment
17. cd ~/Drigan/environment/
18. source bin/activate
    You can skip 19 and 20 point and optionally clone repo over HTTPS:
    git clone https://zeroos@bitbucket.org/zeroos/drigan.git
19. Set up SSH Key. Tutorial here: https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678
20. git clone git@bitbucket.org:zeroos/drigan.git
21. cd drigan
22. cp drigan/settings_example.py drigan/settings.py
23. vim drigan/settings.py
24. pip install -r requirements.txt
25. sudo su - postgres
26. createdb drigan
27. See: `Postgresql priveleges configuration`_
28. sudo service postgresql restart
29. python ./manage.py syncdb
30. python manage.py collectstatic
31. python manage.py runserver
