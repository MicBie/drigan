from django.forms import Form, ModelForm


class DriganForm(Form):
    error_css_class = "form-error"
    required_css_class = 'form-required'


class DriganModelForm(ModelForm):
    error_css_class = "form-error"
    required_css_class = 'form-required'
