��    "      ,  /   <      �  %   �  !       A     S     f     �     �     �  k   �     7  5   G     }  
   �     �     �     �     �     �            !   2     T     ]     l     �     �  
   �     �  F   �     �  &     -   ,  ,   Z  �  �  %   -	  I  S	     �
     �
  "   �
     �
  $   �
  #     r   C     �  3   �     �       #        @     Z     g  $   �     �     �  %   �                    >     K  
   \     g  K   o     �      �  +   �  *                                            !                                     "                                                                   
       	       
Sincerely, 
%(sitename)s Management
 
You (or someone pretending to be you) have asked to register an account at %(sitename)s.

If this wasn't you, please ignore this email and your address will be removed from our records.

To activate this account, please click the following link within the next  %(expiration_days)s days:
 Account activated Account activation Account activation failed Account activation on Account successfully activated Activation email sent An activation email has been sent.  Please check your email and click on the link to activate your account. Change password Email with password reset instructions has been sent. Forgot password Not member Organize your event with us! Password changed Password reset Password reset failed Password reset successfully Registration closed Registration completed Registration is currently closed. Reset it Reset password Reset password at %(site_name)s Sign in Sign up Signed out Submit Type in your email and we will send you a link to reset your password. Upcoming events You have been successfully signed out. Your account has been successfully activated. Your password has been successfully changed. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-26 13:08+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
Pozdrawiamy 
Zespół %(sitename)s 
 
Otrzymałeś tę wiadomość ponieważ Twój adres e-mail został użyty do rejestracji na %(sitename)s.

Jeśli nie zakładałeś konta zignoruj tę wiadomość- Twój adres zostanie automatycznie usunięty z naszej bazy danych.

Żeby aktywować konto kliknij w poniższy link w ciągu najbliższych  %(expiration_days)s dni:
 Konto zostało aktywowane Aktywacja konta Aktywacja konta nie powiodła się Aktywacja konta Konto zostało pomyślnie aktywowane E-mail aktywacyjny został wysłany E-mail aktywacyjny został wysłany.  Prosimy sprawdzić pocztę i klknąć w podany link, żeby aktywować konto. Zmień hasło E-mail z instrukcją zmiany hasła został wysłany Zapomniałeś hasła Nie masz konta Zorganizuj z nami swoje wydarzenie! Hasło zostało zmienione Reset hasła Reset hasła nie powiódł się Hasło zostało pomyślnie zmienione Rejestracja zamknięta Rejestracja zakończona Rejestracja jest aktualnie zamknięta Zresetuj hasło Zmień hasło Zmiana hasła na %(site_name)s Zaloguj się Zarejestruj się Wylogowano Wyślij Wpisz adres e-mail, na który mamy wysłać instrukcje jak zmienić hasło. Nadchodzące wydarzenia Zostałeś pomyślnie wylogowany Twoje konto zostało pomyślnie aktywowane. Twoje hasło zostało pomyślnie zmienione 