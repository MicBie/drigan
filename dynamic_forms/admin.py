from django.contrib import admin
from dynamic_forms.models import DynamicFormField, DynamicForm, DynamicFormData, \
    DynamicFormFile

admin.site.register(DynamicForm)
admin.site.register(DynamicFormField)
admin.site.register(DynamicFormData)
admin.site.register(DynamicFormFile)
