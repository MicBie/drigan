��    ;      �  O   �           	  
          	   5      ?     `      v     �     �     �  #   �     �  (        +     7     >  	   K  "   U     x     �  $   �     �  	   �  	   �  
   �     �  "     3   (     \  B   l     �     �     �     �     �                          /     I  7   e     �  Z   �  S   	     k	  $   x	  
   �	     �	     �	     �	  
   �	     �	     �	     �	     �	     
     
  �  

  	   �     �     �  
   �  .   �  "        B     X     _  )   }      �     �  '   �                    4     E     e  	   }  #   �     �     �     �     �  -   �  (     6   C     z  E   �     �     �               %     9     @  
   O  "   Z     }     �  G   �  )   �  W   &  O   ~     �  -   �     	          -     5     =      F     g     z     �     �     �     3          .      2   '                 :   !          6                   ,          0       -      4   5             1           #      /           +       (   )      
                             &   	               *          9      $   8   "         %   7                    ;    A form Add choice Add choice to your field: Add field Attached document or spreadsheet Attached image or pdf Attachment field (any file type) Back Can edit dynamic forms Can view participants lists Choice has been added successfully. ComboBox Field Date Field (Temporarily used char field) Date filled Delete E-mail Field Edit form Field has been added successfully. Field has been deleted. Field type: Field with this name already exists. Field: Fill date Fill form Fill form: Finish editing/reorder fields Form has been filled successfully. Invalid key --- probably stale data in choice field Long text Field Name of the field, it will be displayed as label for this question Number Field Order has been changed. Order: Participants list Participants list: Save Save this field Settings Short text Field There are no participants This choice already exists. This value can't contain leading or trailing whitespace Type of data this field stores Uploaded file is neither an image nor pdf!  
 Supported formats: gif/jpeg/png/svg/tiff/pdf Uploaded file is not a document! 
 Supported formats: doc/docx/xls/xlsx/odt/ods/txt Yes/No Field Your form hasn't got any fields yet. Your form: all participants down everyone field type form can be filled list visibility name required up № Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-20 22:12+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Formularz Dodaj wybór Dodaj wybór do swojego pola: Dodaj pole Załącznik - dokument lub arkusz kalkulacyjny Załącznik - obrazek lub plik pdf Załącznik - dowolny Wróć Czy może edytować formularz Czy może oglądać listę uczestników Wybór został pomyślnie dodany Pole z listą wyboru Pole z datą (tymczasowo pole tekstowe) Data wypełnienia Usuń Pole z adresem e-mail Edytuj formularz Pole zostało pomyślnie dodane Pole zostało usunięte Typ pola: Pole o takiej nazwie już istnieje. Pole: Data wypełnienia Wypełnij formularz Wypełnij formularz: Zakończ edycję lub zmień kolejność pól Formularz został wypełniony pomyślnie Niepoprawny klucz --- zapewne stare dane w polu wyboru Długie pole tekstowe Nazwa pola, która będzie wyświatlana jako etykieta do tego pytania Pole z liczbą Kolejność została zmieniona. Kolejność: Lista uczestników Lista uczestników: Zapisz Zapisz to pole Ustawienia Krótkie pole tekstowe (1 linijka) Nie ma żadnych uczestników Taki wybór już istnieje Ta wartość nie może zawierać bialych znaków na początku i końcu Rodzaj danych, które ma zbierać to pole Ten plik nie jest ani obrazem ani pdf-em! 
Dostępne formaty: gif/jpeg/png/svg/tiff/pdf Ten plik nie jest dokumentem! 
Dostępne formaty: doc/docx/xls/xlsx/odt/ods/txt Pole TAK/NIE Twój formularz nie ma jeszcze żadnych pól. Twój formularz: wszyscy uczestnicy w dół wszyscy typ pola formularz może być wypełniany widoczność listy nazwa wymagane w górę № 