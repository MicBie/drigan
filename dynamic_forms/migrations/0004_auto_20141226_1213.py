# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0003_auto_20141121_1652'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dynamicformdata',
            options={'ordering': ['fill_date', 'pk']},
        ),
        migrations.AddField(
            model_name='dynamicformdata',
            name='fill_date',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 26, 12, 13, 21, 474761), auto_now_add=True),
            preserve_default=False,
        ),
    ]
