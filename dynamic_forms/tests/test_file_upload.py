# -*- coding: utf-8 -*-
from django.test import TestCase
import unittest
from dynamic_forms.models import DynamicFormFile

from .. import models


class TestFilenameSanitizing(unittest.TestCase):

    def test_example_1(self):
        result = DynamicFormFile.clean_filename_for_user("Żaźółć Gęśką Jaźń")
        self.assertEqual(result, "_a____ G__k_ Ja__")

    def test_example_2(self):
        result = DynamicFormFile.clean_filename_for_user("Ala\tma kota.\nA Tola\t\nAsa")
        self.assertEqual(result, "Ala_ma kota__A Tola__Asa")

