# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.test.testcases import TestCase
from dynamic_forms.models import DynamicForm, DynamicFormField


class TestFieldCantContainWhitespace(TestCase):

    def setUp(self):
        self.form = DynamicForm.objects.create()

    def test_field_with_whitespace(self):
        field = DynamicFormField(
            form=self.form,
            name="   TEST   ",
            field_type="DynamicIntegerField",
            position=0)

        try:
            field.full_clean()
        except ValidationError as e:
            self.assertEqual({'name'}, e.args[0].keys())

    def test_field_no_whitespace(self):

        field = DynamicFormField(
            form=self.form,
            name="TEST",
            field_type="DynamicIntegerField",
            position=0)

        field.full_clean()
