# -*- coding: utf-8 -*-
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_variables, \
    sensitive_post_parameters
import collections
from random import SystemRandom
import string

from django.http import HttpResponseRedirect, StreamingHttpResponse
from django.views.generic.base import View, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, \
    DeleteView, CreateView, ModelFormMixin
from django.views.generic.list import ListView
from django import http
from django.core.urlresolvers import reverse
from django.db.transaction import atomic
from django.utils.translation import ugettext as _
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied

from guardian.mixins import LoginRequiredMixin

from dynamic_forms.default_fieldtypes import ChoicesField
from dynamic_forms.forms import AddDynamicFormField, BaseDynamicForm,\
    AddChoices, ChangeDynamicFormPropertiesForm
from dynamic_forms.models import DynamicForm, DynamicFormData, DynamicFormField, \
    FieldNameNotUniqueError, DynamicFormFile
from dynamic_forms.utils.file_storage import DriganFileStorage

from io import StringIO
import csv
from django.http import HttpResponse

from django.views.generic import UpdateView


BackRef = collections.namedtuple("BackRef", ['url', 'label'])


class AddDynamicForm(LoginRequiredMixin, View):

    http_method_names = ['post']

    def post(self, request, content_type_model, object_id):

        content_type = ContentType.objects.get(model=content_type_model)
        get_object_or_404(content_type.model_class(), pk=object_id)
        dynamic_form = DynamicForm(content_type=content_type,
                                   object_id=object_id)

        if not dynamic_form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()

        dynamic_form.save()

        return http.HttpResponseRedirect(reverse(
            'dynamic_forms.views.add_dynamic_form_field',
            args=(dynamic_form.id,)))


class GoBackMixin(ContextMixin):

    def get_backref(self, dynamic_form):
        return None

    def get_context_data(self, **kwargs):
        backref = self.get_backref(self.dynamic_form)
        url = backref.url if backref is not None else None
        label = backref.label if backref is not None else None

        ctx = {
            "dynform_backref_url": url,
            "dynform_backref_label": label
        }

        ctx.update(**kwargs)

        return super().get_context_data(**ctx)


class EditDynamicForm(GoBackMixin, LoginRequiredMixin, UpdateView, ModelFormMixin):

    http_method_names = ['get', 'post']
    pk_url_kwarg = 'dynamic_form_id'
    model = DynamicForm
    template_name = "dynamic_forms/dynamic_form_edit.html"
    context_object_name = "dynamic_form"
    form_class = ChangeDynamicFormPropertiesForm

    def dispatch(self, request, *args, **kwargs):
        form = self.get_object()
        if not form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()
        self.dynamic_form = form
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.path

    def get_context_data(self, **kwargs):
        context = {'action': self.request.path}
        context.update(kwargs)
        return super(EditDynamicForm, self).get_context_data(**context)


class ChangeFieldOrder(LoginRequiredMixin, View):

    http_method_names = ['post']

    def post(self, request, field_id, direction):
        field = get_object_or_404(DynamicFormField, pk=field_id)

        if not field.form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()

        field.position += int(direction)
        field.save()
        messages.success(request, _('Order has been changed.'))
        return http.HttpResponseRedirect(reverse(
            'dynamic_forms.views.edit_dynamic_form',
            args=(field.form.id,)))


class AddDynamicFormFieldView(GoBackMixin, LoginRequiredMixin, CreateView):

    template_name = "dynamic_forms/dynamic_form_add.html"
    form_class = AddDynamicFormField

    def dispatch(self, request, *args, **kwargs):
        self.dynamic_form = get_object_or_404(DynamicForm,
                                              pk=kwargs['dynamic_form_id'])

        if not self.dynamic_form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'dynamic_form': self.dynamic_form,
            'dynamic_form_form': BaseDynamicForm(self.dynamic_form)
        })
        return kwargs

    def get_success_url(self):
        return reverse(
            'dynamic_forms.views.add_dynamic_form_field',
            kwargs={"dynamic_form_id": self.dynamic_form.id})

    def form_valid(self, form):
        field = form.save(commit=False)
        try:
            self.dynamic_form.add_field_to_form(field)
            field.save()
            messages.success(self.request,
                             _('Field has been added successfully.'))
        except FieldNameNotUniqueError:
            messages.error(self.request,
                           _('Field with this name already exists.'))
            return self.form_invalid(form)

        if field.field_type == 'DynamicChoicesField':
            # TODO: This should really be handled somewhere else
            return http.HttpResponseRedirect(reverse(
                'dynamic_forms.views.add_choices_to_choicefield',
                args=(field.id,)))

        return HttpResponseRedirect(self.get_success_url())


class DeleteDynamicFormField(LoginRequiredMixin, DeleteView):

    http_method_names = ['post']
    pk_url_kwarg = 'field_id'
    model = DynamicFormField

    def delete(self, request, *args, **kwargs):
        field = get_object_or_404(DynamicFormField,
                                  pk=kwargs[self.pk_url_kwarg])

        if not field.form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()
        result = super().delete(request, *args, **kwargs)
        messages.success(request, _('Field has been deleted.'))
        return result

    def get_success_url(self):
        return reverse('dynamic_forms.views.edit_dynamic_form',
                       args=(self.object.form.pk,))


class AddChoicesToChoiceField(LoginRequiredMixin, FormView):

    http_method_names = ['get', 'post']
    pk_url_kwarg = 'field_id'
    model = DynamicFormField
    form_class = AddChoices
    template_name = "dynamic_forms/choices_add.html"

    def dispatch(self, request, *args, **kwargs):
        self.choice_field = get_object_or_404(DynamicFormField,
                                              pk=kwargs['field_id'],
                                              field_type=ChoicesField.FIELD_NAME)
        self.dynamic_form_id = self.choice_field.form.id
        if not self.choice_field.form.can_edit_dynamic_form(request.user):
            raise PermissionDenied()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            dynamic_form_id=self.dynamic_form_id,
            field_id=self.kwargs['field_id'],
            **kwargs
        )

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        new_choice = form.cleaned_data['name']
        choice_field = self.choice_field
        if not choice_field.dynamic_field.has_choice(choice_field, new_choice):
            choice_field.dynamic_field.add_choice(choice_field, new_choice)
            choice_field.save()
            messages.success(self.request,
                             _('Choice has been added successfully.'))
            return super().form_valid(form)
        else:
            messages.error(self.request,
                           _('This choice already exists.'))
            return super().form_invalid(form)


class FillForm(FormView):

    form_class = BaseDynamicForm
    template_name = "dynamic_forms/form_fill.html"

    def get_form_id(self):
        """ Returns `id` of used form. """
        return self.kwargs['dynamic_form_id']

    def get_success_url(self):
        return reverse('dynamic_forms.views.participants_list',
                       args=(self.dynamic_form.pk,))

    @method_decorator(sensitive_post_parameters())
    @method_decorator(sensitive_variables())
    def dispatch(self, request, *args, **kwargs):
        self.dynamic_form = get_object_or_404(DynamicForm,
                                              pk=self.get_form_id())
        if not self.dynamic_form.can_be_filled:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['dynamic_form'] = self.dynamic_form
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        if not user.is_authenticated():
            rand = SystemRandom()
            rand_name = "".join([rand.choice(string.ascii_letters + string.digits) for __ in range(15)])
            username_pattern = "anonymous-{}".format(rand_name)
            user = User(
                username=username_pattern
            )
            user.set_unusable_password()
            user.save()

        with atomic():
            dfd = DynamicFormData.objects.create(form=self.dynamic_form,
                                                 user=user,
                                                 data={})

            data = form.cleaned_data

            serialized_data = {
                f.name: f.dynamic_field.serialize_field_content(
                    f.name, data.get(f.name, None), form, self.request, dfd)
                for f in self.dynamic_form.fields.all()}
            dfd.data = serialized_data
            dfd.save()

        messages.success(self.request,
                         _('Form has been filled successfully.'))
        return super().form_valid(form)


class ParticipantList(ListView):

    http_method_names = ['get']
    template_name = "dynamic_forms/participants_list.html"
    context_object_name = "participants"

    @method_decorator(sensitive_post_parameters())
    @method_decorator(sensitive_variables())
    def dispatch(self, request, *args, **kwargs):
        self.dynamic_form = get_object_or_404(DynamicForm,
                                              pk=kwargs['dynamic_form_id'])

        if not self.dynamic_form.can_see_participants_list(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return DynamicFormData.objects.filter(form=self.dynamic_form)


class SaveList(LoginRequiredMixin, View):

    http_method_names = ['get']

    @method_decorator(sensitive_post_parameters())
    @method_decorator(sensitive_variables())
    def get(self, *args, **kwargs):
        form = get_object_or_404(DynamicForm, pk=kwargs['dynamic_form_id'])
        fields = form.fields.all()
        participants = DynamicFormData.objects.filter(form=kwargs['dynamic_form_id'])

        buffer = StringIO()
        csvWriter = csv.writer(buffer)
        headers = [_("Date filled")]
        headers.extend([f.name for f in fields])
        csvWriter.writerow(headers)
        for p in participants:
            row = [p.fill_date]
            row.extend([a[1] for a in p.data_ordered])
            csvWriter.writerow(row)
        csv_file = buffer.getvalue()
        buffer.close()
        response = HttpResponse()
        response['Content-Disposition'] = 'attachment; filename="' + 'file.csv' + '"'
        response['Content-Type'] = 'text/csv'
        response.write(csv_file)
        return response


class ServeFileFromDynamicForm(DetailView):

    model = DynamicFormFile

    @method_decorator(sensitive_post_parameters())
    @method_decorator(sensitive_variables())
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        if not self.object.data.form.can_see_uploaded_files(self.request.user):
            raise PermissionDenied()

        response = StreamingHttpResponse()

        file_name = DynamicFormFile.clean_filename_for_user(self.object.file_name_for_user)

        response['Content-Disposition'] = 'attachment; filename="{}"'.format(file_name)
        response['Content-Type'] = self.object.content_type
        storage = DriganFileStorage()
        file = storage.open(self.object.file_name_in_storage)
        response.streaming_content = file
        return response
