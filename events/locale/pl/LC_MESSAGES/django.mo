��    E      D  a   l      �  0   �  +   "  @   N  -   �     �  	   �     �  '   �  )        >     J     V     h  '   u     �     �  
   �     �  	   �  "   �  $        2     G  !   a     �     �  )   �  	   �     �     �     �     	     )	  f   /	     �	     �	     �	     �	     �	     �	     
  7   	
     A
     I
  
   Z
     e
     q
     z
     �
     �
     �
     �
     �
     �
     �
       
   	               $     )  	   2     <     K     X  
   ^     i     �  @  �  1   �  2   �  B   /  A   r     �     �     �  $   �  '        @     I     U     f  %   x     �     �     �     �     �  &     )   (     R  #   c     �     �     �  8   �     �               *     F     Y  X   a     �     �     �     �          .     7  D   J     �     �     �     �  	   �     �     �     �     �     �               !  
   3     >  
   O     Z     b     h     m     y     �     �     �     �     �               *      ?             A             #       ,   "       D   6           (   %       .           B      )   &      2   7   /      4   @                             !      	      <   
          C   $   :          3                  E   1           5   0                '                       -      9                     >          +   =   8   ;    
        Delete attraction named "%(name)s"
     
        Delete event named "%(name)s"
     
    Do you really want to delete attraction named: "%(name)s"?
 
   Do you really want to delete "%(name)s"?
 Add attraction Add event Add registration form Attraction has been added successfully. Attraction has been changed successfully. Attractions Change logo Delete attraction Delete event Done editing form, return to attraction Edit attraction Edit attraction: Edit event Edit event - %(event)s Edit form Event has been added successfully. Event has been changed successfully. Form data incorrect. Logo changed successfully Logo not changed - no data given. No upcoming events... None :( Only if event is cyclic (eg. "1", "2014") Organizer Organizer details Organizer details: Other editions of this event Participants list Place Please provide date and time according to the following example {example}. You may omit the time part. Return to attraction edition Return to event edition Save Save as csv Sign up for this attraction! Unknown Website Without edition (eg. "Long Race", not "Long Race 2013") address all participants attraction attractions category creator creator's mail description of the attraction description of the event e-mail address edition name end date end date of the event event event name events everyone name none yet organizer organizer only phone number place start date start date of the event website Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-02-14 10:42+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
        Usuń atrakcję o nazwie "%(name)s"
     
        Usuń wydarzenie o nazwie "%(name)s"
     
    Czy na pewno chcesz usunąć atrakcję o nazwie: "%(name)s"?
 
   Czy na pewno chcesz usunąć wydarzenie o nazwie "%(name)s"?
 Dodaj atrakcję Dodaj wydarzenie Dodaj formularz rejestracyjny Atrakcja została pomyślnie dodana. Atrakcja została pomyślnie zmieniona. Atrakcje Zmień logo Usuń atrakcję Usuń wydarzenie Zakończ edycję i wróć do atrakcji Edytuj atrakcję Edytuj atrakcję Edytuj wydarzenie Edytuj wydarzenie - %(event)s Edytuj formularz Wydarzenie zostało pomyślnie dodane. Wydarzenie zostało pomyślnie zmienione. Niepoprawne dane Logo zostało pomyślnie zmienione. Logo nie zostało zmienione Brak wydarzeń... Brak :( Tylko jeśli wydarzenie jest cykliczne (np. "1", "2014") Organizator Detale organizatora Detale oranizatora: Inne edycje tego wydarzenia Lista uczestników Miejsce Dodaj datę w formacie zgodnym z tym przykładem: {example}. Możesz pominąć godzinę. Powróć do edycji atrakcji Powróć do edycji wydarzenia Zapisz Zapisz jako plik csv Zapisz się na to wydarzenie! Nieznany Strona internetowa Bez nazwy edycji (np. "Długi Wyścig", a nie "Długi Wyścig 2013") adres wszyscy uczestnicy atrakcja atrakcje kategoria twórca e-mail twórcy opis atrakcji opis wydarzenia adres e-mail nazwa edycji data końcowa koniec wydarzenia wydarzenie nazwa wydarzenia wydarzenia wszyscy nazwa brak organizator tylko organizator numer telefonu miejsce data początkowa początek wydarzenia strona internetowa 