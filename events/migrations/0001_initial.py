# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
from django.conf import settings

class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attraction',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('deleted_at', models.DateTimeField(null=True, blank=True, default=None)),
                ('name', models.CharField(verbose_name='name', max_length=200)),
                ('start_date', models.DateTimeField(verbose_name='start date')),
                ('end_date', models.DateTimeField(verbose_name='end date')),
                ('logo', models.ImageField(null=True, blank=True, upload_to='uploads/events/logos')),
                ('place', models.CharField(verbose_name='place', max_length=200)),
                ('description', models.TextField(null=True, verbose_name='description of the attraction', blank=True)),
            ],
            options={
                'ordering': ['start_date', 'name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AttractionCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=100)),
                ('slug', models.SlugField(verbose_name='slug')),
                ('active', models.BooleanField(verbose_name='active', default=True)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('thumbnail', models.ImageField(null=True, blank=True, upload_to='uploads/categories/thumbnails')),
            ],
            options={
                'verbose_name_plural': 'attraction categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('deleted_at', models.DateTimeField(null=True, blank=True, default=None)),
                ('name', models.CharField(help_text='Without edition (eg. "Long Race", not "Long Race 2013")', verbose_name='event name', max_length=200)),
                ('start_date', models.DateTimeField(null=True, verbose_name='start date of the event', blank=True)),
                ('end_date', models.DateTimeField(null=True, verbose_name='end date of the event', blank=True)),
                ('logo', models.ImageField(null=True, blank=True, upload_to='uploads/events/logos')),
                ('website', models.URLField(verbose_name='website', blank=True, max_length=100)),
                ('description', models.TextField(null=True, verbose_name='description of the event', blank=True)),
                ('edition', models.CharField(help_text='Only if event is cyclic (eg. "1", "2014")', verbose_name='edition name', blank=True, max_length=100)),
            ],
            options={
                'abstract': False,
                'permissions': (('can_undelete', 'Can undelete this object'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=200)),
                ('mail', models.EmailField(verbose_name='e-mail address', max_length=75)),
                ('phone', models.CharField(verbose_name='phone number', max_length=20)),
                ('address', models.TextField(null=True, verbose_name='address', blank=True)),
                ('related_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SerialEventGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SportCategory',
            fields=[
                ('attractioncategory_ptr', models.OneToOneField(auto_created=True, to='events.AttractionCategory', primary_key=True, parent_link=True, serialize=False)),
            ],
            options={
                'verbose_name_plural': 'sport categories',
            },
            bases=('events.attractioncategory',),
        ),
        migrations.AddField(
            model_name='event',
            name='category',
            field=models.ForeignKey(to='events.SportCategory', verbose_name='category'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='event_group',
            field=models.ForeignKey(to='events.SerialEventGroup', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='organizer',
            field=models.ForeignKey(to='events.Organizer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attractioncategory',
            name='parent',
            field=mptt.fields.TreeForeignKey(to='events.AttractionCategory', null=True, verbose_name='parent', blank=True, related_name='children'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attraction',
            name='category',
            field=models.ForeignKey(to='events.AttractionCategory', verbose_name='category'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attraction',
            name='event',
            field=models.ForeignKey(to='events.Event'),
            preserve_default=True,
        )
    ]
