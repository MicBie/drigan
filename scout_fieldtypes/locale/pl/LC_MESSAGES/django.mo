��    	      d      �       �   
   �      �      �   �        �     �            �  &     �     �     �  �   �     �     �     �     �                             	                 NIP number PESEL number PESEL or NIP number Please provide either a valid NIP number or a valid PESEL number. PESEL number should be provided as 11-digit string: 01234567891. NIP can be specified either as 10-digit string or as: XXX-XXX-XX-XX, XXX-XX-XX-XXX. Polish County Polish ID card number Polish Province Postal number Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-26 22:02+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 NIP PESEL PESEL lub NIP Proszę, wprowadź prawidłowy numer NIP lub prawidłowy numer PESEL. Numer PESEL powinien składać się z 11-cyfrowego ciągu: 01234567891. NIP może być podany jako 10-cyfrowy ciąg lub w formie: XXX-XXX-XX-XX, XXX-XX-XX-XXX. Powiat Numer dowodu osobistego Województwo Kod pocztowy 